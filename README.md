# NODeJ

NODeJ is written in Java and provided as an imageJ java.trax.plugin and a CLI option

**Table of content**

[[_TOC_]]

### What is NODeJ?

Link to the paper : https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-022-04743-6

NODeJ is an automated method, developed as an imageJ java plugin, to segment and analyze high intensity domains in 
nuclei from two or three dimensional images. This new bioinformatic tool applies laplacian method on the mask of a 
nucleus to enhance the contrast of intra-nuclear objects and allow their segmentation. NODeJ allows for efficient
 automated analysis of subnuclear structures by avoiding the semi-automated steps, resulting in reduced processing
 time and analytical bias. NODeJ is written in Java and provided as an ImageJ plugin with a command line option to 
 perform more high-throughput analyses.

<div align="center">
<img src="img/Cj.png"   width="50%" height="50%" /> 
</div>

***NODeJ workflow**; the program takes as input the raw (A) and segmented image (B) of the nucleus and computes the 
laplacian image (C). A. Raw image of a plant nucleus ( A. thaliana ) at interphase stained with DAPI. B. Image of the 
segmented nucleus obtained with NucleusJ2.0. C. Image of the laplacian obtain with NODeJ. High voxel values are shown in
 red and low values are shown in blue. D. The resulting segmented image, in which each object (connected component) can 
 be analyzed individually.*

### Citing NODeJ

Dubos T, Poulet A, Thomson G, Péry E, Chausse F, Tatout C, Desset S, van Wolfswinkel JC, Jacob Y. NODeJ: an ImageJ plugin for 3D segmentation of nuclear objects. BMC Bioinformatics. 2022 Jun 6;23(1):216. doi: 10.1186/s12859-022-04743-6. PMID: 35668354; PMCID: PMC9169307.

### Lab website

### How to install NODeJ?

You can download NODeJ [jar](https://gitlab.com/api/v4/projects/22788291/packages/maven/burp/NODeJ/1.0.5/NODeJ-1.0.5.jar) . There is 2 ways to use it : 

* Use NODeJ with an graphical interface through [fiji](https://fiji.sc)/[imageJ](https://imagej.nih.gov/ij/download.html) 
: download the jar file and install it in fiji/imageJ folder (more details [here](https://imagejdocu.tudor.lu/howto/plugins/how_to_install_a_plugin)).

* Use Command Line Interface (CLI) : NODeJ are available in command line mode. More below.

### NODeJ  CLI help menu:

#### Usage

`java -jar NODeJ.jar [options]`

        
#### Parameters:
   
   * **-f,--factor <arg>**         Factor used to adjust the threshold value on the object final detection
   * **-gX,--gaussianX <arg>**     Gaussian Blur X sigma (default value 1)
   * **-gY,--gaussianY <arg>**     Gaussian Blur Y sigma (default value 1)                         
   * **-gZ,--gaussianZ <arg>**     Gaussian Blur Z sigma (default value 2)                      
   * **-iRaw,--inputRaw <arg>**    folder containing raw images
   * **-iSeg,--inputSeg <arg>**    folder containing segmented images
   * **-isF,--isFilter**           If used, the connected component will be filter on the min and max size parameter                    
   * **-isG,--isGaussian**         If used, Applied Gaussian Filter on Raw images
   * **-max,--maximum <arg>**      Maximum size, if connected component > to max the connected component is suppress (default 3 μm^3)
   * **-min,--minimum <arg>**      Minimum size, if connected component < to min the connected component is suppress (default 0.003 μm^3)
   * **-n,--neighborhood <arg>**  Size of the neighborhood (s) tested for the Laplacian method (default 3 for small object (volume < 50 μm^3) or automatically adjusted for larger nuclei (s = s·2.5).
   * **-o,--output <arg>**         Path to output folder
  

#### Command line eg:

`java -jar NODeJ.jar -raw path/to/raw/folder -seg path/to/rawSeg/folder -o path/to/output/folder [options]`

## Authors:

* **Axel Poulet**: Department of Molecular, Cellular  and Developmental Biology, Yale University, 165 Prospect St 
New Haven, CT 06511, USA. _Contact: pouletaxel@gmail.com_ 
* **Tristan Dubos**: GReD, CNRS, INSERM, Université Clermont Auvergne, Clermont-Ferrand, France. _Contact:
 tristan.duos33@gmail.com_
