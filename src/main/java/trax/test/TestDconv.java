package trax.test;

import ij.IJ;
import loci.plugins.BF;
import trax.FilesInputOutput.FilesNames;
import ij.ImagePlus;
import trax.plugin.ChromocenterParameters;
import trax.process.ChromocenterCalling;
import trax.utils.Rd2ToTif;
import trax.utilsNj2.NucleusChromocentersAnalysis;


import java.io.File;

public class TestDconv {

    public static void main(String[] args) throws Exception {

      //  String input = "/home/plop/Desktop/tskAnaysis/a56_tsk/";
       // String zp =   "/home/plop/Desktop/Images2D/tskAnaysis/a56_tsk/ZProject/";

        //String input = "/home/plop/Desktop/YLab/TSK_microscopic_pictures/atxr56/atxr56-1.nd2";
    //    String output = "/home/plop/Desktop/Yi-Chun_a56tsk/";
       // String psf ="/home/plop/Desktop/PSFDapi-520-520-25RW.tif";
       // String deconvLab2 ="/home/plop/Desktop/DeconvolutionLab_2.jar";
       // java.lang.String zMethod = "avg";

        //File file = new File(output);
        //if(!file.exists())
          //  file.mkdir();

        //NucleusZproject zProject = new NucleusZproject(input,zp);
        //zProject.run();
  //      String seg = "/home/plop/Desktop/Images2D/tskAnaysis/tsk/seg/";
//        System.out.println("java.trax.test seg");
        //NucleusSegmentation segmentation = new NucleusSegmentation(zp, output);
        //segmentation.run();



        String input = "/home/plop/Desktop/ArcadiaImageAnalysis/WT_X1_CENPA_K9me2/nuclei/C0";
        String inputSeg = "/home/plop/Desktop/ArcadiaImageAnalysis/WT_X1_CENPA_K9me2/nuclei/GIFT/";
        String output = "/home/plop/Desktop/ArcadiaImageAnalysis/WT_X1_CENPA_K9me2/nuclei/WT_X1_CENPA_K9me2";

        /*ImagePlus [] raw = BF.openImagePlus(input);
        ImagePlus [] nuc = BF.openImagePlus(inputSeg);
        ImagePlus [] cc = BF.openImagePlus(inputSeg);
        */

//         -iSeg  -o  -f 3 -n 2 -noC


        ChromocenterParameters CCAnalyseParameters =new  ChromocenterParameters(
                input, inputSeg, output, true, 3,3);

        ChromocenterCalling CCAnalyse= new ChromocenterCalling(CCAnalyseParameters,false);
        CCAnalyse.runSeveralImages2();

        //NucleusChromocentersAnalysis nucleusChromocenterAnalysis = new NucleusChromocentersAnalysis();
        //nucleusChromocenterAnalysis.compute3DParameters("Volume", raw[0], nuc[0],nuc[0],CCAnalyseParameters);



        System.out.println("!!!! End deconv Image");

//            Segmentation seg = new Segmentation(deconvDir + deconvName + ".tif", output);
//            seg.run();
//        }
    }
}
