package trax.process;

import trax.FilesInputOutput.Directory;
import trax.FilesInputOutput.FilesNames;
import ij.IJ;
import ij.ImagePlus;
import loci.common.DebugTools;
import loci.plugins.BF;
import trax.gui.Progress;
import trax.plugin.ChromocenterParameters;
import trax.utilsNj2.NucleusChromocentersAnalysis;

import java.io.File;

public class ChromocenterCalling {
    ChromocenterParameters chromocenterParameters ;


    private String _prefix;
    private boolean _is2DImg =false;
    private boolean _isGui=false;
    private Progress _p;

    /**
     *
     * @param chromocenterParameters
     */
    public  ChromocenterCalling(ChromocenterParameters chromocenterParameters ){
        this.chromocenterParameters=chromocenterParameters;

    }

    public  ChromocenterCalling(ChromocenterParameters chromocenterParameters, boolean gui ){
        this.chromocenterParameters=chromocenterParameters;
        _isGui = gui;
    }

    /**
     *
     * @throws Exception
     */
    public void  runSeveralImages2() throws  Exception{
        DebugTools.enableLogging("OFF");
        Directory directoryInput = new Directory(this.chromocenterParameters.getInputFolder());
        directoryInput.listImageFiles(this.chromocenterParameters.getInputFolder());
        directoryInput.checkIfEmpty();
        String rhfChoice = "Volume";
        String diffDir = this.chromocenterParameters.m_outputFolder+"gradientImage";
        File file = new File (diffDir);
        if(!file.exists()) file.mkdir();

        String segCcDir = this.chromocenterParameters.m_outputFolder+"SegCC";
        file = new File (segCcDir);
        if(!file.exists()) file.mkdir();
        // TODO A REFAIRE C EST MOCHE !!!!
        System.out.println("size: "+ directoryInput.getNumberFiles());

        if (this._isGui){
            _p = new Progress("Images Analysis: ",directoryInput.getNumberFiles());
            _p._bar.setValue(0);
        }
        for (short i = 0; i < directoryInput.getNumberFiles(); ++i) {
            File currentFile = directoryInput.getFile(i);
            String fileImg = currentFile.toString();
            FilesNames outPutFilesNames = new FilesNames(fileImg);
            FilesNames SegCC = new FilesNames(this.chromocenterParameters._segInputFolder +
                    currentFile.separator+currentFile.getName());
            this._prefix = outPutFilesNames.PrefixeNameFile();
            ImagePlus [] _raw = BF.openImagePlus(currentFile.getAbsolutePath());
            //is2D => change
            imageType(_raw[0]);
            String outputFileName= segCcDir+currentFile.separator+currentFile.getName();
            String gradientFileName= diffDir+currentFile.separator+currentFile.getName();

            if(SegCC.is_fileExist()) {
                ImagePlus [] segNuc =BF.openImagePlus(this.chromocenterParameters._segInputFolder +
                            currentFile.separator+currentFile.getName());
                    ChromencenterSegmentation chromencenterSegmentation = new ChromencenterSegmentation(
                            _raw,
                            segNuc,
                            outputFileName,
                            this.chromocenterParameters);
                    chromencenterSegmentation.runCC3D(gradientFileName);
                    NucleusChromocentersAnalysis nucleusChromocenterAnalysis = new NucleusChromocentersAnalysis();
                    nucleusChromocenterAnalysis.compute3DParameters(
                            rhfChoice,
                            _raw[0],
                            segNuc[0],
                            IJ.openImage(outputFileName),
                            this.chromocenterParameters);
            }
            else{
                IJ.log(SegCC.get_fullPathFile()+" is missing");
            }
            if(this._isGui){_p._bar.setValue(1 + i);}

        }
        if (this._isGui){
            _p.dispose();
        }
    }

    /**
     *
     * @throws Exception
     */
    public void  just3D() throws  Exception{
        DebugTools.enableLogging("OFF");
        Directory directoryInput = new Directory(this.chromocenterParameters.getInputFolder());
        directoryInput.listImageFiles(this.chromocenterParameters.getInputFolder());
        directoryInput.checkIfEmpty();
        String rhfChoice = "Volume";
        String nameFileChromocenter = this.chromocenterParameters.m_outputFolder+"CcParameters.tab";

        String segCcDir = this.chromocenterParameters.m_outputFolder;

        // TODO A REFAIRE C EST MOCHE !!!!
        System.out.println("size: "+ directoryInput.getNumberFiles());

        for (short i = 0; i < directoryInput.getNumberFiles(); ++i) {
            File currentFile = directoryInput.getFile(i);
            String fileImg = currentFile.toString();
            ImagePlus [] _raw = BF.openImagePlus(currentFile.getAbsolutePath());
            imageType(_raw[0]);
            String outputFileName= segCcDir+currentFile.separator+currentFile.getName();
            ImagePlus [] segNuc =BF.openImagePlus(this.chromocenterParameters._segInputFolder+currentFile.separator+currentFile.getName());
           // ChromocenterAnalysis chromocenterAnalysis = new ChromocenterAnalysis(_raw[0],segNuc[0], IJ.openImage(outputFileName));
           // chromocenterAnalysis.computeParametersChromocenter();

            NucleusChromocentersAnalysis nucleusChromocenterAnalysis = new NucleusChromocentersAnalysis();
            nucleusChromocenterAnalysis.compute3DParameters(
                rhfChoice,
                _raw[0],
                segNuc[0],
                IJ.openImage(outputFileName),
                this.chromocenterParameters);
            }
    }

    /**
     *
     * @param ramImage
     */
    public void imageType(ImagePlus ramImage){
        ramImage.getDimensions();
        if(ramImage.getStackSize()==1){
            this._is2DImg =true;
        }
    }

}
