package trax.utils;

import ij.ImagePlus;
import ij.measure.Calibration;
import ij.plugin.ZProjector;

import java.io.File;

/**
 * 
 */
public class ZProjection {
    private String _input;
    private String _output;

    /**
     *
     */
    public ZProjection(String input, String output){
        this._input = input;
        this._output = output;
    }

    /**
     *
     */
    public void run() throws Exception {
        String [] t = _input.split(File.separator);
        String nameZ = t[t.length-1].replace("deconv.tif","zp.tif");
        ImagePlus img2D = this.getZProjection();

        Rd2ToTif.saveFile(img2D,_output);
    }


    /**
     *
     * @return
     */
    public ImagePlus getZProjection() throws Exception {

        ImagePlus img = Rd2ToTif.getImageChannel(0,_input);
        //img.show();
        Calibration cal = img.getCalibration();
        ZProjector zp = new ZProjector(img);
        zp.setMethod(ZProjector.MAX_METHOD);
        zp.doProjection();
        ImagePlus img2D = zp.getProjection();
        img2D.setCalibration(cal);
        return img2D;
    }
}
