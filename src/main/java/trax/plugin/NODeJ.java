package trax.plugin;

import trax.gui.GuiAnalysis;
import ij.plugin.PlugIn;
import ij.IJ;

import trax.process.ChromocenterCalling;

/**
 * Method to detect the chromocenters on bam
 *
 * @author Tristan Dubos and Axel Poulet
 *
 */
public class NODeJ implements PlugIn {

    /**
     *
     * @param arg
     */
    public void run(String arg) {
        GuiAnalysis gui = new GuiAnalysis();
        while (gui.isShowing()) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (gui.isStart()) {
            ChromocenterParameters CCAnalyseParameters = new  ChromocenterParameters(
                    gui.getInputRaw(),
                    gui.getInputSeg(),
                    gui.getOutputDir(),
                    gui.getGaussianX(),
                    gui.getGaussianY(),
                    gui.getGaussianZ(),
                    gui.isGaussian(),
                    gui.isFilter(),
                    gui.getMax(),
                    gui.getMin());

            ChromocenterCalling CCAnalyse= new ChromocenterCalling(CCAnalyseParameters);
            try {
                CCAnalyse.runSeveralImages2();
            } catch (Exception e) { e.printStackTrace(); }
            IJ.log("End of the chromocenter segmentation , the results are in ");
        } else {
            IJ.showMessage("There are no the two subdirectories (See the directory name) or subDirectories are empty");
        }
    }

}
