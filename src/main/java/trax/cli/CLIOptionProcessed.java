package trax.cli;
import org.apache.commons.cli.*;

/**
 *
 */
public class CLIOptionProcessed {

    /** */
    Options _options= new Options();
    /** */
    CommandLine  _cmd;
    /** */
    Option _inputRaw = Option.builder("iRaw").longOpt("inputRaw").required()
            .type(String.class).desc("folder containing raw images\n")
            .numberOfArgs(1).build();

    Option _inputSeg = Option.builder("iSeg").longOpt("inputSeg").required()
            .type(String.class).desc("folder containing segmented images\n")
            .numberOfArgs(1).build();
   /** */
    Option _outputFolder= Option.builder("o").longOpt("output").required()
            .type(String.class).desc("Path to output folder \n")
            .numberOfArgs(1).build();
    /** */
    Option _gaussianX = Option.builder("gX").longOpt("gaussianX")
            .type(Number.class).desc("Gaussian Blur X sigma (default value 1) \n")
            .numberOfArgs(1).build();

    /** */
    Option _gaussianY = Option.builder("gY").longOpt("gaussianY")
            .type(Number.class).desc("Gaussian Blur Y sigma (default value 1) \n")
            .numberOfArgs(1).build();



    /** */
    Option _gaussianZ = Option.builder("gZ").longOpt("gaussianZ")
            .type(Number.class).desc("Gaussian Blur Z sigma (default value 2) \n")
            .numberOfArgs(1).build();

    /** */
    Option _isGaussian = Option.builder("isG").longOpt("isGaussian").hasArg(false)
            .type(boolean.class).desc("If used, Applied Gaussian Filter on Raw images \n")
            .numberOfArgs(0).build();
    /** */
    Option _isFilterCC = Option.builder("isF").longOpt("isFilter").hasArg(false)
            .type(boolean.class).desc("If used, the connected component will be filter on the min and max size parameter \n")
            .numberOfArgs(0).build();

    Option _noChange = Option.builder("noC").longOpt("noChange").hasArg(false)
            .type(boolean.class).desc("If used, the f and n parameter will not change on function of the nucleus size \n")
            .numberOfArgs(0).build();

    /** */
    Option _min = Option.builder("min").longOpt("minimum")
            .type(Number.class).desc("Minimum size, if connected component < to min the connected component is suppress (default 0.003 μm^3)\n")
            .numberOfArgs(1).build();
    /** */
    Option _max = Option.builder("max").longOpt("maximum")
            .type(Number.class).desc("Maximum size, if connected component > to max the connected component is suppress (default 3 μm^3)\n")
            .numberOfArgs(1).build();

    Option _neigh = Option.builder("n").longOpt("neighborhood")
            .type(Number.class).desc("Size of the neighborhood tested for the Laplacian method (default 3 for small object (volume < 50 μm^3)," +
                    " or automatically adjusted for larger nuclei (s = s·2.5)\n")
            .numberOfArgs(1).build();

    Option _factor = Option.builder("f").longOpt("factor")
            .type(Number.class).desc("Factor used to adjust the threshold value on the object final detection\n")
            .numberOfArgs(1).build();


    /**
     * Command line parser
     */
    CommandLineParser _parser= new DefaultParser();


    public CLIOptionProcessed()  {

    }

    /**
     *
     * @param args
     * @throws Exception
     */
    public CLIOptionProcessed(String[] args)  {
         /*required parameters*/
        this._options.addOption(_inputRaw);
        this._options.addOption(_outputFolder);
        this._options.addOption(_inputSeg);


        /*optional parameters*/

        this._options.addOption(_isGaussian);
        this._options.addOption(_gaussianX);
        this._options.addOption(_gaussianY);
        this._options.addOption(_gaussianZ);

        this._options.addOption(_isFilterCC);
        this._options.addOption(_noChange);
        this._options.addOption(_max);
        this._options.addOption(_min);
        this._options.addOption(_factor);
        this._options.addOption(_neigh);


        try {
            _cmd = _parser.parse(this._options, args,false);
        }
        catch (ParseException  exp ){
            System.out.println("\n"+exp.getMessage()+"\n");
            CLIHelper.CmdHelp();
         }catch (IllegalArgumentException exp){
            System.out.println( exp.getMessage());
            System.exit(1);
        }

    }

    /**
     *
     * @return
     */
    public CommandLine getCommandLine() {
        return _cmd;
    }

    /**
     *
     * @return
     */
    public Options getOptions() {
        return _options;
    }

}
