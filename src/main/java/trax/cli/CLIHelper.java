package trax.cli;

import org.apache.commons.cli.HelpFormatter;
public class CLIHelper {

    /** */
    private static String _version ="1,0.3";

    /** */
    private static String _egCommand ="java -jar NODeJ."+ _version +".jar ";


    /* Constructor*/

    public CLIHelper(){  }


    public static void main(String[] args){

        CmdHelp();
    }



    /**
     * Method get help for command line
     * with example command line
     */
     public static void CmdHelp() {
        String argument =  "-iRaw path/to/input/RawImages/folder -iSeg path/to/input/SegImages/folder -o path/to/output/folder ";
        String[] argv = argument.split(" ");
         CLIOptionProcessed command = new CLIOptionProcessed (argv);
        String usage = _egCommand+argument+" [options]";
        HelpFormatter formatter = new HelpFormatter();

        System.out.println("\nHelp for "+_egCommand+"!!!!!!! \n");
        formatter.printHelp(200, usage, "\nNODeJ version "+_version+" option:", command.getOptions(),getAuthors());
        System.exit(1);
     }


    /**
     *
     * @return
     */
    public static String getAuthors() {
        return  "Authors:\n" +
                "Axel Poulet\n" +
                "Department of Molecular, Cellular  and Developmental Biology Yale University \n" +
                "Tristan Dubos\n" +
                "GReD, CNRS, INSERM, Université Clermont Auvergne, Clermont-Ferrand, France\n" +
                "Contact:\ntristan.duos33@gmail.com OR pouletaxel@gmail.com\n";
    }


}
